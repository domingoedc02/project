import { Form, Button, Container, Row, Col } from "react-bootstrap"
import UserContext from "../UserContext"
import { useState, useEffect, useContext } from "react";

export default function Login(){
    const { setUser } = useContext(UserContext)

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)

    useEffect( () => {
        if(email !== "" && password !== ""){
            setIsDisabled(false)
        }
        else{
            setIsDisabled(true)
        }
    }, [email, password])

    function Login(e){
        e.preventDefault()

        localStorage.setItem("email", email)
        setUser({
            email: localStorage.getItem('email')
        })

        console.log(email)
        
        alert("Successfully Login", )
    }
    


    return(
        <Container>
            <Row className=" m-3 ">
                <Col md={{ span: 6, offset: 3 }} className="border p-3">
                    <Form>
                        <Form.Group className="mb-3" controlId="email">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                                type="email" 
                                placeholder="Enter email" 
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Password" 
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                />
                        </Form.Group>
                        <Button variant="primary" type="submit" disabled={isDisabled} onClick={Login}>
                            Submit
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}