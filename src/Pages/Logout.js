import { useContext, useEffect } from "react"
import { Redirect } from "react-router-dom/cjs/react-router-dom.min"
import UserContext from "../UserContext"

export default function Logout(){

    const {unsetUser, setUser} = useContext(UserContext)
        unsetUser()
        useEffect(() => {
        setUser({email: null})
        }, [])
    return (
        <Redirect to={"/"}/>
    )
}