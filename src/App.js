import { useState } from "react";
import Routes from "./Components/Routes";
import { UserProvider } from "./UserContext";




function App() {

  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  })
  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
        <Routes/>
    </UserProvider>
    
  );
}

export default App;
