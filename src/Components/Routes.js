import { BrowserRouter, Switch, Route } from 'react-router-dom/cjs/react-router-dom.min';
import { Container } from 'react-bootstrap';

import AppNavbar from './AppNavbar';
import Login from '../Pages/Login';
import Home from '../Pages/Home';
import Logout from '../Pages/Logout';

export default function Routes(){
    return (
        <BrowserRouter>
            <AppNavbar />
                <Container fluid className='m-3'>
                    <Switch>
                        <Route exact path={"/"} component={Home}/>
                        <Route exact path={"/login"} component={Login}/>
                        <Route exact path={"/logout"} component={Logout}/>
                        <Route component={Home}/>
                    </Switch>
                </Container>
        </BrowserRouter>
    )
}