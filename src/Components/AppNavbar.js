import { Navbar, Container, Nav, NavDropdown  } from "react-bootstrap"
import 'bootstrap/dist/css/bootstrap.min.css';
import { NavLink } from "react-router-dom/cjs/react-router-dom.min";
import { Fragment, useContext } from "react";
import UserContext from "../UserContext";

export default function AppNavbar(){

    const { user } = useContext(UserContext)

    let nav = (user.email != null) ? 
        <Nav.Link as={NavLink} to={'/logout'}>Logout</Nav.Link>
    :
        <Fragment>
        <Nav.Link as={NavLink} to={'/register'}>Register</Nav.Link>
        <Nav.Link as={NavLink} to={'/login'}>Login</Nav.Link> 
        </Fragment>

    return(
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container fluid>
                <Navbar.Brand href="#home">Eya Ganda</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link href="#features">Features</Nav.Link>
                        <Nav.Link href="#pricing">Pricing</Nav.Link>
                        <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
                            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>

                    <Nav>
                        {nav}  
                    </Nav>

                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}